# yade-data

This repository contains the data files for YADE. It is a submodule of `yade-dev/trunk` in the directory `./data`. You can clone `yade-dev/trunk` along with this submodule using command:

	git clone --recurse-submodules git@gitlab.com:yade-dev/trunk.git

If you already have a cloned yade repository, then you can populate your existing repository using command:

	git submodule update --init --recursive

If you want to make changes in here (here means: `./data` : the repository `yade-dev/yade-data`) then adding commits without a branch is problematic.
So first please make sure to be on some branch, most likely `main` (\*):

	git checkout main


Then you can update YADE datafiles and push to `yade-dev/yade-data`, when ready go to the parent directory and connect recent data commit hash with `yade-dev/trunk`:

	cd ../
	git add ./data
	git commit

Then the checkout of `yade-dev/trunk` with `--recurse-submodules` will make sure that proper "connected" commit hash is present in that directory. The limitation is that this commit
has to be present in the git history of `yade-dev/yade-data`.

It is also possible to tell git to [track a branch](https://stackoverflow.com/questions/1777854/how-can-i-specify-a-branch-tag-when-adding-a-git-submodule/18799234#18799234) instead
of tracking commit hashes, but so far we are not sure which one is better, because we predict (currently) only one branch with data in it.

(\*) Looks like gitlab recently changed default branch name to `main`
