This folder includes data files that are necessary for a correct execution of either [CapillarityEngine](https://yade-dem.org/doc/yade.wrapper.html#yade.wrapper.CapillarityEngine) or [Law2_ScGeom_CapillaryPhys_Capillarity](https://yade-dem.org/doc/yade.wrapper.html#yade.wrapper.Law2_ScGeom_CapillaryPhys_Capillarity) when describing capillary bridges in YADE.

CapillarityEngine
-----------------

File `capillaryfile.txt` is provided for using CapillarityEngine.

Law2_ScGeom_CapillaryPhys_Capillarity
-------------------------------------

The set of `M(r=*)` files is intended for Law2_ScGeom_CapillaryPhys_Capillarity. Each file is for a given radius ratio `r`=Rmax/Rmin for the two capillary-bonded spheres. A generic line in a file includes capillary bridge properties in a dimensionless form and throughout 8 columns: (notations and Equation labels refer to [Duriez2017](https://link.springer.com/article/10.1007/s11440-016-0500-6), see also a fulltext copy [there](https://hal.archives-ouvertes.fr/hal-01868741))

- interparticle distance $`d^*`$, see text above Eq. (7)
- capillary pressure $`u_c^*`$, see Eq. (2)
- volume $`V^*`$, see text above Eq. (7)
- force $`F^*`$, see Eq. (9)
- half-filling angle $`\delta_1`$ (on the smallest particle) in degrees, see Fig. 1
- half-filling angle $`\delta_2`$ (on the biggest particle) in degrees, see Fig. 1
- radial component of the axisymmetric tensor $`\pi_ij^*`$, see Eq. (10)
- axial component of the same tensor $`\pi_ij^*`$, see Eq. (10)

Additional lines delimit some file structure that is internally used by the engine, corresponding to the existence of a given pattern in distance and capillary pressure values (e.g., L2 of `M(r=1)` corresponds to the consideration of 79 distance values in that file, while L3 reflects the existence of 350 different bridge data, i.e. 350 following lines, all sharing the same 0 distance value).

General remarks
---------------

For both engines, the bridge properties of a given YADE interaction are interpolated from those included in the files and no bridge is computed when YADE parameters are outside the values covered in the data files. For instance, no bridge can be computed by Law2_ScGeom_CapillaryPhys_Capillarity for an interaction showing a radius ratio strictly greater than 10.